/*
savecodeshare.eu website
Copyright (C) 2018 FSFE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// SPDX-License-Identifier: AGPL-3.0-or-later

var DESKTOP_BREAKPOINTS = {
    height: 720,
    width: 1280
};

var MOBILE_BREAKPOINT = {
    height: 720,
    width: 767
};

$(document).ready(function () {
    var stickyHeader = $('#sticky-header');
    var bodyHeight = $('body').height();
    var signupImageSource = $('#image_photo').attr('src');

    $('.w-reviews-slider').slick({
        slidesToScroll: 1,
        slidesToShow: 1,
        autoplay: true,
        dots: false,
        arrows: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    arrows: false,
                    dots: true
                }
            }
        ]
    });

    var anchor = window.location.hash;
    if (!!anchor && $(anchor).length > 0 && $(anchor).hasClass('hidden')) {
        $('<a></a>').addClass('hidden').attr('href', anchor).modal();
    }

    $('.w-main-nav .nav-link').click(function () {
       closeMenu();
    });

    $('a[href^="#"]:not(a[data-modal], a[rel="modal:close"])').click(function(e) {
        e.preventDefault();

        var me = $(this);
        var selector = $(this).attr('href');
        var from = $(window).scrollTop();
        var to =  $(selector).offset().top;
        var speed =  Math.abs(to - from) / bodyHeight * 700;

        if (me.parents('.w-main-nav').length > 0) {
            closeMenu();
            $('html, body').delay(300);
        }

        $('html, body').animate({
            scrollTop: to
        }, speed);
    });

    $('a[data-modal]').each(function () {
        var me = $(this);
        var selector = me.data('modal');
        me.attr('href', selector).click(function (e) {
            e.preventDefault();
            me.modal();
        });
    });
    $('[data-modal-close]').click(function () {
        $.modal.close();
    });

    $('section.hidden')
        .on($.modal.BEFORE_CLOSE, function(event, modal) {
            if (!!anchor && anchor.slice(1) === $(this).attr('id')) {
                removeHash();
            }
        })
        .on($.modal.BEFORE_OPEN, function (event, modal) {
            clearModal(modal.$elm, true);
        })
    ;

    $('#sign_type').change(function () {
        var $modal = $(this).parents('.modal');
        clearModal($modal);
    });

    $('input[name=photo]').change(function (e) {
        var me = $(this);
        var input = e.target;
        var label = me.parent();
        label.find('.image-placeholder').hide();
        var isOrganisation = label.hasClass('organisation');
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            if (isOrganisation) {
                var image =  label.find('img').first();
                reader.onload = function (e) {
                    image.attr('src', e.target.result).show();
                };
            } else {
                reader.onload = function (e) {
                    label.css('background-image', 'url(' + e.target.result + ')');
                };
            }
            reader.readAsDataURL(input.files[0]);
        }
    });

    $('.tooltip').tooltipster({
        side: ['bottom'],
        trigger: 'custom',
        triggerOpen: {
            mouseenter: true,
            tap: true
        },
        triggerClose: {
            mouseleave: true,
            click: true,
            tap: true,
            scroll: true
        }
    });

    $('.right-tooltip').tooltipster({
        side: ['right']
    });

    $('form[name="signupForm"]').submit(function (e) {
        var me = $(this);
        var error = me.find('.js-privacy-error');
        var checkbox = me.find('.js-privacy-checkbox');
        if (!!checkbox.prop('checked')) {
            error.hide();
            return;
        }
        error.show();
        e.preventDefault();
    });

    function toggleSignupSubmit() {
        var emptyFields = true;
        submit.removeClass('success', emptyFields).attr('disabled', emptyFields);
    }

    function handleStickyHeader() {
        if ($(document).width() >= MOBILE_BREAKPOINT.width) {
            $(window).scroll(function () {
                var topValue = $(window).scrollTop() > stickyHeader.height() ? '0' : '-100%';
                (stickyHeader.css('top') !== topValue) && stickyHeader.css('top', topValue);
            });
        } else {
            $(window).off('scroll');
        }
    }

    function closeMenu() {
        $('#hamburger').prop('checked', false);
    }

    function removeHash() {
        history.pushState('', document.title, window.location.pathname + window.location.search);
    }

    function clearModal($modal, isOpening) {
        $modal.find('.js-privacy-error').hide();
        $modal.find('input, select, textarea').not('[type=hidden], .hidden').each(function () {
            var me = $(this);
            var type = me.attr('type');

            if (type === 'radio' || type === 'checkbox') {
                me.prop('checked', false);
                return;
            }

            me.val('');
        });
        if ($modal.hasClass('js-sign-modal')) {
            var uploader = $modal.find('.c-image-upload');
            uploader.removeAttr('style');
            uploader.find('.image-placeholder').show();
            uploader.hasClass('organisation') && uploader.children('img').first().hide();

            if (!!isOpening) {
                $modal.find('[name="receive_information"]').prop('checked', false);
            }
        }
    }

    $(window).resize(function () {
        handleStickyHeader();
    }).trigger('resize');
});
